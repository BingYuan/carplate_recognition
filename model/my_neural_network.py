import keras.backend as K
from keras.layers import Input, Dense, Conv2D, Flatten, MaxPooling2D, Dropout, Activation, GlobalAveragePooling2D, concatenate
from keras.models import Model
from keras.models import Sequential
from keras.optimizers import SGD, Adam
    
def my_neural_network(input_width, input_height, input_channel, nb_class, weights=None):
  model = Sequential()
  model.add(Conv2D(64, (3, 3), activation='relu',input_shape=(28,28,1)))
  model.add(MaxPooling2D(pool_size=(3,3)))
  model.add(Flatten())
  model.add(Dense(10, activation='softmax'))
  if weights != None:
    model.load_weights(weights)
  return model