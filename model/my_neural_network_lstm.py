import keras.backend as K
from keras.layers import Input, Dense, Conv2D, Flatten, MaxPooling2D, Dropout, Activation, GlobalAveragePooling2D, concatenate,LSTM
from keras.layers import Embedding,TimeDistributed,  BatchNormalization, Reshape , RepeatVector,  GRU
from keras.models import Model
from keras.models import Sequential
from keras.optimizers import SGD, Adam
    
def my_neural_network_weichen(input_shape=(55,110,3),nb_class=36, nb_char=7):
	inputs = Input(shape=input_shape)
	hidden = Conv2D(16, (5, 5), padding='same', activation='relu')(inputs)
	hidden = Conv2D(32, (5, 5), padding='same', activation='relu')(hidden)
	hidden = BatchNormalization(axis=3, momentum=0.1, epsilon=1e-5)(hidden)
	hidden = Conv2D(32, (5, 5), padding='same', activation='relu')(hidden)
	hidden = Conv2D(64, (5, 5), padding='same', activation='relu')(hidden)
	hidden = Conv2D(64, (5, 5), padding='same', activation='relu')(hidden)
	hidden = MaxPooling2D(pool_size=(3,3))(hidden)
	hidden = Dense(12)(hidden)
		
	hidden = Flatten()(hidden)
	hidden = Dense(128)(hidden)
	hidden = Reshape((128,1))(hidden)
	hidden = LSTM(32)(hidden)
	hidden = RepeatVector(nb_char)(hidden)
	hidden = LSTM(10,return_sequences=True)(hidden)
	output = TimeDistributed(Dense(nb_class, activation='softmax'))(hidden)
	model = Model(inputs=inputs, output=output)

	return model
                             
