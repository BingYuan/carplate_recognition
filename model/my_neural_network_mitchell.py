import keras.backend as K
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
%matplotlib inline
from keras.datasets import mnist
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.optimizers import Adam
from keras.layers.normalization import BatchNormalization
from keras.utils import np_utils
from keras.layers import Conv2D, MaxPooling2D, ZeroPadding2D, GlobalAveragePooling2D
from keras.layers.advanced_activations import LeakyReLU 
from keras.models import Model
from keras.models import Sequential
from keras.optimizers import SGD, Adam

def my_neural_network(input_width, input_height, input_channel, nb_class, weights=None):
  model = Sequential()

  model.add(Conv2D(32, (3, 3), input_shape=(28,28,1)))
  model.add(Activation('relu'))
  model.add(BatchNormalization(axis=1))
  model.add(Conv2D(32, (3, 3)))
  model.add(Activation('relu'))
  model.add(MaxPooling2D(pool_size=(2,2)))
#   model.add(BatchNormalization(axis=-1))
#   model.add(Conv2D(64,(3, 3)))
#   model.add(Activation('relu'))
#   model.add(BatchNormalization(axis=-1))
#   model.add(Conv2D(64, (3, 3)))
#   model.add(Activation('relu'))
#   model.add(MaxPooling2D(pool_size=(2,2)))

  model.add(Flatten())
  # Fully connected layer

  model.add(Dense(512))
  model.add(Activation('relu'))
  model.add(BatchNormalization())
  model.add(Dropout(0.2))
  model.add(Dense(10 ,activation='softmax'))

  return model

# model.add(Convolution2D(10,3,3, border_mode='same'))
# model.add(GlobalAveragePooling



