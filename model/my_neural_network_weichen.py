import keras.backend as K
from keras.layers import Input, Dense, Conv2D, Flatten, MaxPooling2D, Dropout, Activation, GlobalAveragePooling2D, concatenate
from keras.models import Model
from keras.models import Sequential
from keras.optimizers import SGD, Adam

def my_neural_network_weichen(input_width, input_height, input_channel, nb_class, weights=None):
  model = Sequential()
  model.add(Flatten(input_shape=(28, 28, 1)))
  model.add(Dense(256, activation='sigmoid',name='fc1'))
  model.add(Dropout(0.5))
  model.add(Dense(10, activation='sigmoid'))
  if weights != None:
    model.load_weights(weights)
	
  return model