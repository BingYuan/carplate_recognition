from PIL import Image, ImageDraw, ImageFont
import random
import string
import argparse
import numpy as np
import os
parser = argparse.ArgumentParser(description='Generate numpy file which contain image and label generated using text')
parser.add_argument('--width',default=110, type=int)
parser.add_argument('--height',default=55, type=int)
parser.add_argument('--seed', default=9001 ,type=int)
parser.add_argument('--count', default=60000 ,type=int)
parser.add_argument('--postfix', default="train")

args = parser.parse_args()

W = args.width
H = args.height
random.seed(args.seed) 
counter = 0
count = args.count
X = []
y = []
fonts = ['Arial_Bold.ttf', 'Charles_Wright.ttf', 'FE.ttf', 'UK.ttf', 'ptf.ttf']
if os.path.exists(os.path.join(os.path.dirname(__file__),'data','X_{}.npy'.format(args.postfix))):
	print('{}ing set exists'.format(args.postfix))
else:
	print('Generating {}'.format(args.postfix))
	for fonttype in fonts:
		for x in range(int(count/len(fonts))):
			img = Image.new('RGB',(W,H), color=(0,0,0))
			prefix = ''.join(random.choice(string.ascii_uppercase) for _ in range(3))
			postfix = ''.join(random.choice(string.digits) for _ in range(4))
			message = '{} {}'.format(prefix,postfix)
			font = ImageFont.truetype(os.path.join(os.path.dirname(__file__),'font',fonttype).replace('/',"\\"), 20)
			d = ImageDraw.Draw(img)
			w, h = d.textsize(message, font=font)
			d.text(((W-w)/2,(H-h)/2), message, fill="white", font=font)
			# img.save('img'+str(counter).zfill(5)+'.jpg')
			counter+=1
			X.append(np.array(img))
			y.append('{}{}'.format(prefix,postfix))

	X = np.asarray(X)
	y = np.asarray(y)
	print(X.shape,y.shape)
	print("Done")
	np.save(os.path.join(os.path.dirname(__file__),'data','X_{}'.format(args.postfix)),X)
	np.save(os.path.join(os.path.dirname(__file__),'data','y_{}'.format(args.postfix)),y)

