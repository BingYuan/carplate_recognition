import numpy as np
import os

def load_data():
	return ( 
			np.load(os.path.join(os.path.dirname(__file__),'data','X_train.npy')),
			np.load(os.path.join(os.path.dirname(__file__),'data','y_train.npy'))
		), ( 
			np.load(os.path.join(os.path.dirname(__file__),'data','X_test.npy')),
			np.load(os.path.join(os.path.dirname(__file__),'data','y_test.npy')) 
		)
