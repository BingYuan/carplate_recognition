import argparse
import os
import numpy as np
import tensorflow as tf
import keras
from model.my_neural_network import my_neural_network
from model.my_neural_network_hafiz import my_neural_network_hafiz
from model.my_neural_network_mitchell import my_neural_network_mitchell
from model.my_neural_network_weichen import my_neural_network_weichen
from model.my_neural_network_Qadek import my_neural_network_Qadek
import pandas as pd
import seaborn as sns
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from keras.utils import to_categorical
import matplotlib.pyplot as plt

###################################################
# Load data to x_train, y_train, x_test, y_test here
# Chage the following to load data
###################################################
from keras.datasets import mnist
# from text_image_generator.text_image import load_data
# (x_train, y_train), (x_test, y_test) = load_data()
(x_train, y_train), (x_test, y_test) = mnist.load_data()
###################################################
# Loading ends here
###################################################


###################################################
# Input shape should be reshape into 4 dimension as the image are grayscale
# grayscale image normally has a shape of (width, height) but the input dimension should
# be (samples, width, height, channel) which in this case is (samples, width, height, 1)
# Reshape x_train and x_test into 4 dimension in the space below
###################################################
x_train = np.reshape(x_train,(60000,28,28,1))
x_test = np.reshape(x_test,(10000,28,28,1))


print(x_train.shape)
print(x_test.shape)
###################################################
# should print out 60000, 28, 28, 1
###################################################
# Reshaping ends here
###################################################
input_shape = x_train[0].shape

###################################################
# One-hot encode the y label here
###################################################
import keras.utils
from keras import utils as np_utils

number_of_class = 10

y_train = np_utils.to_categorical(y_train, number_of_class)
y_test = np_utils.to_categorical(y_test, number_of_class)

print("Training label now has a shape of {} x {}".format(y_train.shape[0],y_train.shape[1]))
print("Testing label now has a shape of {} x {}".format(y_test.shape[0],y_test.shape[1]))

###################################################
# should print out 60000 x 10 and 10000 x 10
###################################################

###################################################
# Try different optimizer and loss 
###################################################
model = my_neural_network(28,28,1,number_of_class) 
model.compile(optimizer='sgd',
              loss='binary_crossentropy',
              metrics=['accuracy'])

###################################################
# Try adding epoch or changing the batch_size
###################################################

history = model.fit(x_train.reshape(x_train.shape[0],28,28,1), y_train, 
                    validation_split=0.2, 
                    epochs=10,
                    batch_size=128)

score = model.evaluate(x_test, y_test, verbose=0)
print("Test loss: {} , Test accuracy: {}".format(score[0], score[1]))


y_predict = model.predict(x_test)

target_names = ["0","1","2","3","4","5","6","7","8","9"]
ground_truth = np.argmax(y_test,axis=1)
prediction = np.argmax(y_predict,axis=1)
report = classification_report(ground_truth, prediction, target_names=target_names)
print("My neural network")
print(report)
confusion = confusion_matrix(ground_truth, prediction)

dataframe_cm = pd.DataFrame(confusion, index = [i for i in "0123456789"],
                  columns = [i for i in "0123456789"])
plt.figure(figsize = (10,7))
sns.heatmap(dataframe_cm, annot=True, cmap='Blues', fmt='g')
plt.show()

####################################
# Save model into json & h5py file 
####################################
model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
model.save_weights("weight.h5")
print("Saved model to disk")