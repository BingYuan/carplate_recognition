import argparse
import os
import numpy as np
import tensorflow as tf
import keras
from model.my_neural_network import my_neural_network
from model.my_neural_network_hafiz import my_neural_network_hafiz
from model.my_neural_network_mitchell import my_neural_network_mitchell
from model.my_neural_network_weichen import my_neural_network_weichen
from model.my_neural_network_Qadek import my_neural_network_Qadek
import pandas as pd
import seaborn as sns
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from keras.utils import to_categorical
import matplotlib.pyplot as plt

###################################################
# Load data to x_train, y_train, x_test, y_test here
# Chage the following to load data
###################################################

from text_image_generator.text_image import load_data
(x_train, y_train), (x_test, y_test) = load_data()
###################################################
# Loading ends here
##################################################

###################################################
# Input shape should be reshape into 4 dimension as the image are grayscale
# grayscale image normally has a shape of (width, height) but the input dimension should
# be (samples, width, height, channel) which in this case is (samples, width, height, 1)
# Reshape x_train and x_test into 4 dimension in the space below
##################################################
print(x_train.shape)
print(x_test.shape)
###################################################
input_shape = x_train[0].shape

###################################################
# should print out 60000 x 10 and 10000 x 10
###################################################
import string
def string_vectorizer(strng, alphabet='ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'):
    vector = [[0 if char != letter else 1 for char in alphabet] 
                  for letter in strng]
    return vector
	
	
###################################################
# Try different optimizer and loss 
###################################################
model = my_neural_network_weichen(input_shape=(55,110,3),nb_class=36, nb_char=7)
model.compile(optimizer='sgd',
              loss='binary_crossentropy',
              metrics=['accuracy'])

###################################################
# Try adding epoch or changing the batch_size
###################################################

history = model.fit(x_train.reshape(x_train.shape[0],55，110，3), y_train, 
                    validation_split=0.2, 
                    epochs=10,
                    batch_size=128)

score = model.evaluate(x_test, y_test, verbose=0)
print("Test loss: {} , Test accuracy: {}".format(score[0], score[1]))


y_predict = model.predict(x_test)
